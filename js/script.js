$(document).ready(function () {

    $('div[role="alert"]').hide();

    $('#call-modal-2').click(function () {
        $('#modal-1').modal('hide');
    });

    $('form').submit(function (event) {
        event.preventDefault();
    });

    $('#modal-2').on('hidden.bs.modal', function() {
        $('div[role="alert"]').hide();
        $(this).find('#lost-password')[0].reset();
    });

    $("#myModal2").on('show.bs.modal', function (e) {
        $("#myModal1").modal("hide");
    });
});